// Folders
def workspaceFolderName = "Groovy"
def projectFolderName = "ProjectQuiz"
folder ( workspaceFolderName )
folder ( workspaceFolderName + '/' + projectFolderName )
// Jobs
def job1 = freeStyleJob( workspaceFolderName + '/' + projectFolderName + '/' + "Job1" )
def job2 = freeStyleJob( workspaceFolderName + '/' + projectFolderName + '/' + "Job2" )
dej job3 = freeStyleJob( workspaceFolderName + '/' + projectFolderName + '/' + "Job3" )

job1.with{
	scm {
		git {
			remote {
				url ('https://elaineclariz@bitbucket.org/elaineclariz/cartridge.git')
				branch("*/master")
			}
		}
	}
	publishers{
		downstreamParameterized{
			trigger( workspaceFolderName + '/' + projectFolderName + '/' + "Job2" ){
				condition( "SUCCESS" )
				parameters{
					predefinedProp( "CUSTOM_WORKSPACE",'$WORKSPACE')
				}
			}
		}
	}
}

job2.with{
	parameters{
		stringParam("CUSTOM_WORKSPACE","","")
	steps{
	shell('''#!/bin/sh
	ls -lart "$CUSTOM_WORKSPACE" > $CUSTOM_WORKSPACE/output.txt''')
	}
	}
	publishers{
		downstreamParameterized{
			trigger( workspaceFolderName + '/' + projectFolderName + '/' + "Job3" ){
				condition( "SUCCESS" )
				parameters{
					predefinedProp( "CUSTOM_WORKSPACE",'$CUSTOM_WORKSPACE')
				}
			}
		}
	}
}

job3.with{
	parameters{
		stringParam("CUSTOM_WORKSPACE","","")
	steps{
	shell('''#!/bin/sh
	git config --global user.name "Elaine Mediana"
	git config --global user.email "elaine.c.v.mediana@accenture.com"
	cd $CUSTOM_WORKSPACE
	git add .
	git commit -m "Comment"
	git push''')
	}
	}
}

// Pipeline
def pipeline = buildPipelineView( workspaceFolderName + '/' + projectFolderName + '/' + "Pipeline1" )

pipeline.with{
	title('Pipeline Quiz')
	displayedBuilds(5)
	selectedJob( workspaceFolderName + '/' + projectFolderName + '/' + "Job1" )
	showPipelineParameters()
	refreshFrequency(5)
}
	